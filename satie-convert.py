'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from stereoToMono import stereoToMono
import argparse

parser = argparse.ArgumentParser(description='Stereo to mono batch converter utility.')

parser.add_argument('input_dir',
                    type=str,
                    help='Must provide a directory with the stereo wave files to convert.')

parser.add_argument('output_dir',
                    type=str,
                    help='The directory to write the new mono files.')

args = parser.parse_args()

if args.input_dir:
    inDir = args.input_dir 

if args.output_dir:
    outDir = args.output_dir

stereoToMono.conversion(inDir,outDir)
